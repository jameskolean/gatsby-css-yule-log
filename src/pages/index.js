import * as React from 'react'
import Helmet from 'react-helmet'
import '../styles/index.scss'

const IndexPage = () => {
  return (
    <>
      <Helmet title='css yule log' defer={false}>
        <meta
          name='description'
          content='Gatsby silly site entry - CSS yule log'
        />
        <meta charSet='utf-8' />
        <html lang='en' />
      </Helmet>
      <header>
        <h1>css yule log</h1>
      </header>
      <main>
        <div className='log'>
          <div className='bark bark1' />
          <div className='bark bark2' />
          <div className='bark bark3' />
        </div>
        <div class='fire'>
          <div class='flame-main'>
            <div class='flame'></div>
            <div class='ember emberAnimation'></div>
          </div>
          <div class='flame-left '>
            <div class='flame'></div>
          </div>
          <div class='flame-right '>
            <div class='flame'></div>
          </div>
          <div class='flame-base '>
            <div class='flame'></div>
            <div class='ember emberAnimation'></div>
            <div class='ember emberSlowAnimation'></div>
          </div>
        </div>
      </main>
      <footer>
        Thanks to https://speckyboy.com/flame-effects-code-snippets/
      </footer>
    </>
  )
}

export default IndexPage
